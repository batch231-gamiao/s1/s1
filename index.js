// console.log("Hello World!");

// Global Objects
// Arrays

// let students = [ 
// 	"Tony",
// 	"Peter",
// 	"Wanda",
// 	"Vision",
// 	"Loki"
// ];

// students.push("Thor");
// console.log(students);

// students.unshift("Steve");
// console.log(students);

// students.pop()
// console.log(students);

// students.shift("Steve");
// console.log(students);

// let arrNum = [15,20,25,30,11];

// // check if every item in arrNum is divisible by 5
// let allDivisible;

// arrNum.forEach(num => {
// 	if(num % 5 === 0){
// 		console.log(`${num} is divisible by 5`);
// 	}
// 	else {
// 		allDivisible = false;
// 	}
// })
// console.log(allDivisible);

// arrNum.pop();
// arrNum.push(35);
// console.log(arrNum);

// let divisibleBy5 = arrNum.every(num => {
// 	console.log(num);
// 	return num % 5 === 0;
// })

// console.log(divisibleBy5);

// // Math
// // mathematical constants
// console.log(Math);
// console.log(Math.E); 
// console.log(Math.PI); 
// console.log(Math.SQRT2); 
// console.log(Math.SQRT1_2); 
// console.log(Math.LN2);
// console.log(Math.LN10);
// console.log(Math.LOG2E);
// console.log(Math.LOG10E); 

// // methods for rounding a number to an integer

// console.log(Math.round(Math.PI)); 

// console.log(Math.ceil(Math.PI)); 

// console.log(Math.floor(Math.PI)); 

// console.log(Math.trunc(Math.PI)); 

// // method for returning the square root of a number
// console.log(Math.sqrt(3.14));

// // lowest value in a list of arguments
// console.log(Math.min(-4,-3,-2,-1,0,1,2,3,4));
// console.log(Math.max(-4,-3,-2,-1,0,1,2,3,4));

// let name = "mario";

// console.log(name);

// Activity Solution
let students = ["John", "Joe", "Jane", "Jessie"];

// 1.
	function addToEnd(array, value){
		if(typeof value === 'string'){
			array.push(value);
			return array;
		}
		else {
			return "error - can only add strings to an array";
		}
	}

// 2. 
	function addToStart(array,value){
		if(typeof value === 'string'){
			array.unshift(value);
			return array;
		}
		else {
			return "error - can only add strings to an array";
		}
	}

// 3.
	function elementChecker(array,value){
		if(array.length === 0){
			return "error - passed in array is empty";
		}
		else {
			return true;
		}
	}

// 4. 
	function checkAllStringsEnding(array,value){
		if(array.length === 0){
			return "error - array must NOT be empty";
		}
		else if (!array.every(i => (typeof i === "string"))) {
			return "error - all array elements must be strings";
		}
		else if(typeof value !== "string"){
			return "error - 2nd argument must be of data type string";
		}
		else if(value.length > 1){
			return "error - 2nd argument must be a single character";
		}
		else if(array.every(e => e.charAt(e.length-1) === value)){
			return true;
		}
		else {
			return false;
		}
	}

// 5.
	function stringLengthSorter(array){		
		if(array.every(i => (typeof i === "string"))){
			array.sort((a,b)=> a.length - b.length);	
			return array;
		}
		else {
			return "error - all array elements must be strings";
		}
	}





	
	

